#include "communication_ag.h"
int32_t get_c_enum_e_t_type(int32_t c_enum_name) {
    if (c_enum_name >= START_COMMUNICATION_SETT_ENUM && c_enum_name <= END_COMMUNICATION_SETT_ENUM) {
        return C_G_COMM_TYPE_SETTING;
    }

    if (c_enum_name >= START_COMMUNICATION_EVENT_ENUM && c_enum_name <= END_COMMUNICATION_EVENT_ENUM) {
        return C_G_COMM_TYPE_EVENT;
    }

    if (c_enum_name >= START_COMMUNICATION_RES_ENUM && c_enum_name <= END_COMMUNICATION_RES_ENUM) {
        return C_G_COMM_TYPE_RESULT;
    }

    return END_COMMUNICATION_COMM_TYPE_ENUM;
}
